---
title: 21 Oktober 2020
date: 2020-09-11

---
21 Oktober zal ik een korte presentatie geven bij de StayOkay in Soest over imkeren. Vanwege het Corona virus zal er maximaal plaats zijn voor 16 mensen in de zaal. In een kwartier tot maximaal half uur neem ik je mee door het levenscyclus van de honingbij. Ook geef ik nog een aantal tips om je tuin bij vriendelijk te maken. En deel ik foto's.

Tijdens deze dag kan je potjes honing kopen en foto's bekijken.

Normaal kan je Honing proeven tijdens deze dag, in verband met Corona is dit niet mogelijk.

Mocht je niet helemaal 100% zijn, snotteren, ziek voelen, verhoging hebben, in contact geweest zijn met iemand met Corona... Sla deze keer dan even over.
Mocht het nou zo zijn dat ik om een of andere reden mij niet lekker voel dan zal ik deze presentatie niet laten doorgaan.

Mochten de regels omtrent Corona verder aanscherpen, dan zal ik deze natuurlijk volgen.

* Update: De dag is niet doorgegaan ivm Corona maatregelen

![](/uploads/hitte.jpg)

###### _Bijenvolken kunnen het ook warm hebben, ze ventileren de kast dan door buiten op de kast te zitten._