+++
date = 2021-02-13T12:00:00Z
title = "Behandel ik mijn bijenvolken?"

+++
Het korte antwoord is: Ja, ik gebruik uitsluitend stoffen welke van nature al aanwezig zijn in bijenvolken.

Het lange antwoord is:

Sinds de jaren 80 is de varroamijt in Nederland aanwezig. De varroamijt is door menselijk handelen geïmporteerd vanuit Azië waar deze van nature voorkomt. Er zijn diverse theorieën over hoe ze naar Europa zijn gekomen, maar ik weet het niet zeker dus ga er verder niet op in. De realiteit is dat de varroamijt er nu eenmaal is.

De varroamijt is een parasiet, in onze bijenvolken de grootste reden dat er vele virussen aanwezig zijn welke enorme schade aanrichten aan bijenvolken. Varroamijten dragen deze virussen. Zoals bij ons teken virussen kunnen overdragen, is de varroamijt verantwoordelijk voor het overdragen van meerdere virussen bij de honingbij. 

Verhoudingsgewijs tot een mens zou een varroamijt even groot als een konijn zijn, moet je voorstellen om een parasiet ter grootte van een konijn tussen je schouderbladen te hebben. Moet er niet aan denken! Voor bijen is dit helaas de realiteit. 

**Deformed Wing Virus**

Het meest dodelijke virus is het Deformed Wing Virus (DWV), het kreukel vleugel virus. Jonge net geboren bijen komen uit hun cel compleet, ze hebben iets meer haar waardoor ze dons hebben... maar alles is er. Het DWV zorgt ervoor dat hun vleugels verschrompelt en verkreukeld zijn. Deze bijen kunnen niet vliegen en zullen nooit honing kunnen gaan halen, uit onderzoek is gebleken dat er meer aantastingen zijn in hun ontwikkeling. 

De eerste taak van een werksterbij is bijvoorbeeld de cel waaruit ze geboren is opruimen, hierna komt een periode waarin ze verantwoordelijk is voor het voeren van jonge bijen larfjes. Door de aantasting van dit virus kan ze minder goed andere bijen voeren. Het resultaat is een werkster bij welke gewoon haar deel eet, maar vrij weinig kan terug geven aan de kolonie. Ze kan weinig voeren en honing halen kan ze al helemaal niet. Als een kolonie te veel last heeft van dit virus dan zal het bezwijken in de winter.

**Varoamijten**

Varroamijten zijn parasieten, uit recent onderzoek is gebleken dat ze zich tegoed doen aan de vet delen van de honingbij. Een varroamijt doet dit door vlak voordat een bijencel gesloten wordt bij de bijenlarve te kruipen, tijdens deze periode is de larf kwetsbaar en kan de varroamijt zich tegoed doen aan de larf. In de periode dat deze cel gesloten is vermeerdert deze varroamijt zich. Varroamijten hebben een voorliefde voor de cel van de bijen dar, maar als er geen darren zijn slaan ze een werkstercel niet over. Soms komen er wel 4 mijten uit de cel kruipen. Als deze mijten dan ook nog het virus bij zich dragen dan is het feest helemaal compleet. Een zielig verschrompelde bij is het resultaat.

**Behandelen of niet behandelen**

Varroamijten zijn een redelijk nieuw fenomeen, oorspronkelijk komt het virus van een bijensoort uit Azië en niet uit Europa. Evolutionair gezien heeft de honingbij dus nog geen kans gehad om zich uit zichzelf te verdedigen tegen de mijt. 

Nu zijn er vele meningen en discussies onder imkers, een deel is van mening dat de bij het zelf moet oplossen, een ander deel behandeld zijn bijen en een ander deel is bezig met selecteren van bijen met een betere resistentie. Zo zijn er onderzoeken gaande bij Arista bee research, door een koningin kunstmatig te insemineren met het zaad van darren welke ook goede varroa resistentie hebben zijn ze bezig een resistente bij te kweken. Een ander deel van de imkers zal zelfs van mening zijn dat de varroa zelfs helemaal niet door mensen geïntroduceerd is. In een kamer met 20 imkers zijn er minimaal 21 meningen.

Zelf behandel ik mijn bijen, ik zie het varroa probleem als een die door mensen is veroorzaakt en wij hebben daardoor de verantwoordelijkheid voor onze bijen te zorgen. Maar daarnaast ook voor de hommels, wespen en andere soorten bijen in onze omgeving door de mijt druk laag te houden in een gebied. Varroamijten beperken zich namelijk niet tot onze honingbij, en als wij niet behandelen dan hebben daar wilde bijensoort ook last van. Een deel van mijn behandeling dit jaar zal zijn dat ik de volken ga selecteren waar er minder mijten aanwezig bij zijn. 

**Hoe behandel ik**

Tijdens behandelingen heb ik ervoor gekozen om zuren te gebruiken welke van nature al aanwezig zijn in bijenvolken. Deze zuren maken bijen van zichzelf al aan en is ook in  honing te vinden bij bijenvolken welke onbehandeld zijn. Deze zuren zijn: Mierenzuur en Oxaalzuur. Door dit zuur te verdampen of door deze te verdunnen en de bijen te besproeien behandel je een volk tegen mijten, soms zijn behandelingen wel tot 80% effectief. Helaas is er geen behandeling welke voor 100% alle mijten dood maakt. Gezien er in onze regio ook redelijk veel imkers zijn welke niet behandelen is het eigenlijk ook onmogelijk om bijenvolken geheel varroa vrij te krijgen.

Tijdens het jaar behandel ik 3 keer:

* Voorjaar tijdens het maken van nieuwe bijenvolken, enkel de jonge volken
* Na het afnemen van de honing in Juni
* Winter

**Honing**

Ondanks dat deze stoffen in honing aanwezig zijn van nature, wil ik niets extra toevoegen en dus ook geen stoffen vanuit de behandeling. Ik neem dus geen honing af van bijenvolken welke net behandeld zijn, de honing welke na de winter overblijft gaan naar jonge bijenvolken waar ik het eerste jaar geen honing van oogst.