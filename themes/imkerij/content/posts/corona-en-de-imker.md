+++
date = 2021-02-02T23:00:00Z
draft = true
title = "Corona en de Imker"

+++
2020 was een jaar van de Corona pandemie, veel beroepen en bedrijven zijn geraakt en staan op omvallen. Hobby's en vrije tijd moest anders ingericht worden, het volgen van een dansles, sport. Het kon allemaal niet meer. Deze Post is niet om te zeggen dat wij als imkers het zo erg hebben, het laat de impact op de imkerij hobby wel zien. 

2020 zou ook het jaar zijn waarin ik als Mentor zou gaan helpen bij de beginners cursus, ieder jaar geven vele imkerverenigingen les aan cursisten over alle aspecten welke onderdeel zijn van het imkeren. Binnen onze vereniging is er plaats voor 12 leden per jaar, deze worden begeleid door 1 bijenteelt leraar en 2 vaste mentoren. Deze twee mentoren organiseren de cursus en zijn leden van de vereniging. Daarnaast zijn er andere leden welke ondersteuning geven als mentor. Bij de cursussen delen 2 cursisten 1 volk met een mentor, er zijn in totaal 6 volken welke dus gedeeld worden en vaak zit je tijdens inspectie erg dicht bij elkaar. 1.5 meter afstand houden is niet te doen tijdens deze cursus, hierdoor is de cursus van 2020 verzet naar 2021. 

Naast de cursus geven wij vanuit de vereniging uitleg op diverse evenementen. Elk jaar is de Hemelvaartsmarkt in Soest een evenement welke duizenden bezoekers trekt, daarnaast staat de vereniging op de landelijke imkerijdagen en geeft de vereniging les aan scholen. Zelf sta ik op de Lentemarkt van het Gagelgat, de informatiedag bij de StayOkay. Tijdens deze dagen hebben we soms een demonstratiekast bij ons en verkoop ik honing. Deze dagen zijn allemaal geschrapt. 

Naast dat honing op deze evenementen verkocht worden, verkoop ik ook mijn honing via de winkel bij het Gagelgat en bij de StayOkay Soest. Door het sluiten van deze gelegenheden is er vrij weinig honing verkocht. 

 

 