+++
date = 2021-10-24T22:00:00Z
title = "Er zit een wespen nest in mijn tuin"

+++
Geregeld krijgen wij een belletje dat mensen een "bijennest", danwel een wespennest in de tuin hebben zitten. Het grootste deel van alle meldingen gaan over wespen, dit kost helaas heel veel tijd. 

Imkers doen 99% van alle gevallen niets met wespen. Een imker is bezig met honingbijen en niet met Wespen. 

Wespen hebben hun plek in de natuur, ze eten allerlei insecten en houden daardoor deze populaties in toom. Ze eten bijvoorbeeld spinnen, muggen, vliegen etc. Als je een wespen nest ziet hoeft deze dus niet weg direct, zit hij in een schuur ver van het huis: Lekker laten zitten. 

Zit een nest wel dicht bij huis en veroorzaken ze overlast dan kan je erover nadenken een verdelger te bellen. Een imker zal waarschijnlijk niet helpen, zelf kom ik niet eens langs voor een wespennest. Als ik voor elk wespennest langs moet komen kom ik niet meer toe aan mijn eigen bijen. 

Twijfel je er toch over of het een wespennest is of een bijennest?

* Wespen maken een papieren nest, bijen niet.
* Wespen hebben een taille en zijn feller gekleurd. 
* Bijen komen niet op je eten en drinken af, tenzij je pure suikerwater drinkt.

Twijfel je toch? Lees dan even deze pagina door voor een imker in je omgeving:  
[https://www.bijenhouders.nl/bijenwerk/bijenzwerm](https://www.bijenhouders.nl/bijenwerk/bijenzwerm "https://www.bijenhouders.nl/bijenwerk/bijenzwerm")

Ben je er zeker van dat het een wespennest is? Neem dan even contact op met een verdelger. Dit raad ik alleen aan als je overlast hebt. 

Tip:  
Zodra de winter eraan komt vliegen de koninginnen van de wespen uit en gaan op zoek naar een schuilplek. Is het September of Oktober dan heeft het dus weinig zin meer om te verdelgen, over een paar weken zal het nest toch dood gaan. 