+++
date = 2021-06-03T22:00:00Z
title = "Help! Bijen!"

+++
Er is zojuist een zwerm bij u in de tuin geland, wat kunt u doen? Zal u worden gestoken?

In dit artikel probeer ik vragen te beantwoorden.

Honing bijen net als Hommels, wespen, hoornaars en Solitaire bijen zijn onderdeel van onze natuur. Zwermen is een prachtig fenomeen van onze Nederlandse Natuur.

**_Indien u een bijenzwerm ziet, bel de imkersvereniging._**

Op de voorpagina staan de contactgegevens van onze vereniging in Soest, Amersfoort en Leusden. Imkers zijn hobbyisten, het beroep Imker bestaat in Nederland vrijwel niet. Het kan altijd wat duren tot iemand na zijn werk bij je langs kan komen. 

**_Bijen zwermen elk jaar als het goed met ze gaat._**

Bij het zwermen vinden zoek bijen een plek, soms landen ze in de boom in uw tuin, soms in de schoorsteen of in de spouwmuur.

**_Waar zijn bijen goed voor?_**

Bijen zijn bestuivers, die hap die jij kan nemen van je appels en peren en het overgrote deel van al het voedsel op aarde is bestoven door bijen. Zonder bijen geen eten.

**_Is er reden voor paniek en stress?_**

Bijen zijn vredelievende diertjes maar wel met een angel. Ze zoeken je niet op, ze steken niet vanuit het niets en gaan niet in zwermen achter je aan. Soms duiken dergelijke beelden op vanuit Amerika. Meestal worden bijen met miljoenen op een oplegger gezet en door het hele land gesjouwd om amandelen te bestuiven, of zijn het beelden van de geafrikaniseerde bij. Dit zijn bijen die kwaad zijn een zwerm reageert heel anders.

Dit zijn Amerikaanse beelden en **komen niet in Nederland** voor.

**_Wanneer steken bijen dan wel?_**

Bijen vallen alleen aan als er een dreiging is, ga je voor de kast staan te zwaaien met je armen of trek je de kast open dan kunnen ze steken. Of zoals vroeger toen de klaver nog in gazons groeide, kon je wel eens een bijensteek oplopen als je met blote voeten op een bij of hommel stond.

**_Zwermen steken niet_**

Bijen zwermen hebben zojuist huis en haard verlaten om een nieuwe toekomst op te bouwen. Geen jonge bijen larfjes meer en geen honing meer, ze hebben niets meer te verdedigen. Alles wat ze nog over hebben is de honing in hun maag wat genoeg is om een paar dagen te overleven. Als je de zwerm met rust laat en het overlaat aan de imker dan kan je niets gebeuren.

**_Zwermen in de muur_**

Soms kunnen zwermen in de spouwmuur of in de schoorsteen gaan zitten. Je moet dan de muur afbreken om erbij te komen. Honingbijen brengen geen schade aan je woning, ze benutten de ruimte die vrij is maar knagen geen hout of andere materialen aan zodat je woning beschadigd gaat worden. Vaak isoleren ze juist de ruimte met een goedje:  propolis.

Een bijenzwerm gaat niet actief op jacht, zijn niet uit op je ijsjes en zullen ook je vlees niet komen opeten. Bijen zijn nou eenmaal geen wespen.

Ze zoeken naar bloemen en jij bent niet belangrijk.

Ze veroorzaken mitst ze hoog zitten en niet in een looproute geen enkele overlast. Eens per jaar zullen ze zwermen, in dat geval kan je de ramen dicht houden. Meestal is een zwerm vertrokken binnen enkele minuten.

**_Verdelging_**

Uit opmerkingen naar ons Imkers toe zie ik dat er steeds meer intollerantie is naar alles wat vliegt, sommige mensen kunnen het bijna niet aan als er een zwerm in de tuin geland is. We krijgen dan op hoge poten berichtjes dat we **NU** de zwerm moeten komen ophalen.

Soms kiezen mensen voor verdelgen **(wat verboden is volgens een aantal verdelging websites doen ze daarom niet aan verdelging)**, maar de rottende bijen en honingraten trekken andere ongediertes aan zoals Kakkerlakken en wasmotlarfen welke wel schade aanbrengen aan je woning. Daarnaast zullen andere bijen de honing proberen weg te halen uit het dood gespoten volk. Het resultaat is dat je niet alleen de bijen in je muur zojuist hebt verdelgd, maar ook de hommels in de buurt, de honingbijen van de imker.

**Indien er een verdelging heeft plaatsgevonden welke mijn bijenvolken aantast zal ik altijd mijn bijen laten testen en de kosten verhalen op de partij welke de verdelging heeft uitgevoerd. De honing, de volken, mijn materialen kan ik allemaal vernietigen.**

Bijenvolken welke bestreden zijn kunnen wij herkennen aan dat ze kruipend de kast uit gaan in grote getalen, in hun laatste poging hun volk te redden door zich op te offeren door te sterven buiten de kast.

**_Vraag aan jou_**

Een bijenvolk kan in 3 kilometer omtrek vliegen en dus ook zwermen.

Als Nederland vol gebouwd blijft worden op het huidige tempo en ze mogen van jou niet in de tuin of in de schoorsteen wonen... Waar dan wel?

Hoeveel ruimte is er in Nederland denk jij waar geen huizen gebouwd zijn met een omtrek van 3km?