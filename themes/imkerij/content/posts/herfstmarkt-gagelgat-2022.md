+++
date = 2021-09-23T18:00:00Z
title = "Herfstmarkt Gagelgat 2021"

+++
Zaterdag is het zo ver, de Herfstmarkt op Boerderij het Gagelgat zal plaats vinden. 

De markt zal zijn van 11 tot 4 uur en ik zal hier staan met een stand om honing te verkopen. Dit is het enige moment dit jaar dat ik zelf honing zal verkopen. 

Naast dat ik honing verkoop, zal ik met informatie materiaal staan en spullen die ik gebruik in mijn imker hobby. Omdat de bijen de Winter nu ingaan zal ik geen demonstratie kast met bijen bij mij hebben. 

Mocht je niet kunnen Zaterdag, dan is mijn honing ook te koop via:

Boerderij het Gagelgat

StayOkay te Soest

[Tuinderij de Voortuin]()