+++
date = 2021-06-01T22:00:00Z
title = "Honing dit voorjaar"

+++
Dit voorjaar is het koud geweest. Volken hebben hierdoor geen honing binnen gehaald. Er was zelfs niet genoeg voor de volken zelf en ik heb moeten bijvoeren. Van andere imkers heb ik begrepen dat volken welke niet gevoerd zijn soms zijn gesneuveld. Zelf heb ik naast het voeren de honingkamers op de kasten laten staan, alle honing in het voorjaar dat volken haalden was dus voor de bijen volken zelf. En de volken welke geen honing kamers hadden kregen voer. We gaan zien of de Acacia en de Lindeboom dit jaar iets gaan doen. Het belangrijkste is voor mij niet de oogst, maar dat ik volgend jaar nog bijen heb. Een honing oogst en mooie dracht is altijd meegenomen, maar niet het belangrijkste.