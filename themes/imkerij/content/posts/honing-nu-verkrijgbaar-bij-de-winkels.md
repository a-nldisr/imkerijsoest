+++
date = 2021-08-26T22:00:00Z
title = "Honing nu verkrijgbaar bij de winkels!"

+++
Afgelopen weekend heb ik de potjes gevuld en vanaf deze week zal de honing verkrijgbaar zijn bij: 

StayOkay Soest

Boerderij 't Gagelgat

Tuinderij de Voortuin

Dit jaar heb ik minder honing uit mijn bijenvolken gekregen dan vorig jaar. Het was regenachtig en koud, niet ideaal voor bijen. 

As imker ga ik nu het volgende jaar in, het imker jaar start in Augustus. We voeren onze varroa behandeling uit en beginnen het voeren voor winter zodat de volken goed de winter kunnen doorkomen. 