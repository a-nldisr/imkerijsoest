+++
date = 2021-08-16T06:00:00Z
title = "Honing van Juli"

+++
Het zag er even naar uit dat er geen honing zou komen dit jaar, de voorjaar honing was weg geregend. Er was letterlijk geen 1 dag dat mijn bijen konden vliegen. De wintervoorraad is daardoor gebruikt door de volken en ik heb bij moeten voeren. Bijen volken doen het uitstekend op pure sucrose kristalsuiker, bijen zijn niet zoals mensen: Hoe zuiverder de suiker hoe beter voor de bij. Helaas kwam er zeer weinig pollen binnen, dat is iets minder leuk voor de bij en ik heb daardoor pollen supplementen gevoerd. Normaal maken we kunstzwermen of koninginnen tijdens de voorjaarsdracht in Mei. Dit jaar heb ik het telen van koninginnen uitgesteld tot halverwege Juni. 

Eind Mei/Begin Juni kwam toen de Acacia. Van deze honing hebben mijn volken hun voorraad weer op kunnen bouwen, Volken groeiden weer en helaas was er nog erg veel regen. In totaal heeft 1 volk veel honing kunnen halen, deze honing heb ik verdeeld over volken welke er slechter aan toe waren. Alsnog heeft het veel geregend tijdens de bloei van de Acacia, toch was er tussen de buien door tijd voor de bloemen om op te drogen en nectar aan te maken. Na de Acacia hebben we de enige week van echt warm zomers weer gehad dit jaar. Helaas was de Acacia uit gebloeid en was er dus niets te halen. Tijdens deze periode heb ik koninginnen geteeld voor Ameland. 

Na deze week ging het weer regenen en begon de Linde bloei. In totaal bloeit de linde 3-4 weken, helaas heeft het veel geregend maar toch hadden de bijen 5 dagen in totaal om honing te verzamelen. De volken waren sterk en ik heb nog een paar weken de honing laten staan maar afgelopen weken was het dan zo ver, de dop heide kwam in bloei dus ik heb de honing afgenomen. In vergelijking met vorig jaar een fractie aan honing mogen slingeren van mijn volken maar er is iets aan honing. 

Binnenkort zal je de honing bij de verkoop punten terug kunnen vinden van: 

* t Gagelgat
* StayOkay te Soest
* Tuinderij de Voortuin