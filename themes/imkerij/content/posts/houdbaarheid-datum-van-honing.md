+++
date = 2020-09-17T22:00:00Z
title = "Houdbaarheid datum van Honing"

+++
Zelfs in graftombes van Pharao's zijn eetbare potten honing gevonden. Hoe kan het dan dat er een houdbaarheidsdatum op onze moderne potten honing staan? Doen wij Imkers iets met de honing? Nee!

Vanuit de Europese wetgeving zijn wij verplicht om aan een aantal regels te voldoen, dit is goed voor ons als imkers en voor de klant. Het zorgt namelijk ervoor dat honing altijd aan dezelfde regels moeten voldoen.

1 van deze regels is dat de officiële houdbaarheidsdatum van honing 2 jaar is, dit is vanaf het moment dat wij de honing slingeren. Na deze van 2 jaar periode neemt de kwaliteit van de pollen, enzymen en aminozuren in de honing af.

**Hanteer je de 2 jaar ook?**

Zelf hanteer ik niet de 2 jaar houdbaarheidsdatum. Bij mijn honing hanteer ik een houdbaarheidsdatum op basis van het vochtgehalte in de honing. Hoe meer vocht er in een pot honing zit, hoe meer kans er is dat deze kan gaan vergisten. Voor de honing in de potten gaat meet ik het vochtgehalte in de honing, op basis van deze gegevens stel ik de houdbaarheidsdatum vast. Deze is altijd korter dan de wettelijke 2 jaar termijn. Het jaartal dat ik de honing geslingerd heb staat in het lotnummer op de pot. Honing boven 20% vochtgehalte mag niet verkocht worden.

**Is de honing na 2 jaar nog eetbaar?**

De honing blijft nog eetbaar. Het kan wel gebeuren dat de honing kan verkleuren of gaan gisten. Als de honing een bruisende schuimlaag heeft dan is deze aan het vergisten, je kan het dan gebruiken om honing wijn te maken. Maar als imker is mijn enige advies om de honing binnen de termijn op te eten.

Zoals ooit eens een mede imker mooi tegen mij zei:

> Je vreet het maar op.