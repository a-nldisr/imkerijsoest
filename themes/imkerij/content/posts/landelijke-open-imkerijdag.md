+++
date = 2022-07-04T22:00:00Z
title = "Landelijke Open Imkerijdag 2022"

+++
Zaterdag is het dan zo ver, de eerste Landelijke open Imkerijdag zal weer plaats vinden na 2 jaar dit niet hebben kunnen doen. 

Je bent welkom op Boerderij het Gagelgat, op deze dag heb ik een demonstratie kast staan, heb ik een kraam met imker materiaal en kan je vragen stellen. Ook heb ik honing te koop. 

Gagelgat 

Birkstraat 107 in Soest: 

[https://maps.app.goo.gl/ojL1sE1GuBHfWbYJ7](https://maps.app.goo.gl/ojL1sE1GuBHfWbYJ7 "https://maps.app.goo.gl/ojL1sE1GuBHfWbYJ7")

Meer info kan je hier vinden:

[https://www.bijenhouders.nl/landelijke-open-imkerijdag/show/2022/149](https://www.bijenhouders.nl/landelijke-open-imkerijdag/show/2022/149 "https://www.bijenhouders.nl/landelijke-open-imkerijdag/show/2022/149")