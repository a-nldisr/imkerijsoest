+++
date = 2020-09-17T22:00:00Z
title = "Versuikerde honing vloeibaar maken"

+++
Dit jaar was de honing van de paardenbloem en het fruit zoals de appel en kers al na 2 weken versuikerd. Elke honing kan gaan versuikeren, dit kan lang of kort duren. Een reden daarvan kan zijn dat er meer glucose dan dat er fructose in de honing zit. Een andere oorzaak kan zijn dat de honing in contact is geweest met versuikerde honing, dit is bijvoorbeeld hoe imkers creme honing maken.

Natuurlijk versuikerde honing geeft vaak een vervelend mond gevoel, dit is de enige reden om hier iets aan te doen. 

**Versuikerde honing is nog steeds goed en kan nog gegeten worden.** Versuikerde honing hoeft dus niet weggegooid te worden.

Als je honing versuikerd is, gooi deze dan niet weg. Je kan deze au-bain marie verhitten in een pannetje. Als je deze tot 40 C verwarmd dan blijft de kwaliteit van de honing goed, ga je over deze temperatuur dan is er sprake van kwaliteitsverlies. Vergeet bij het verwarmen niet de deksel van de pot af te halen, anders kan hij barsten. Soms hoor ik van mensen dat even een paar tellen in de magnetron stoppen, helaas kan je hiermee niet de temperatuur reguleren en hiermee kook je dus vaak de honing kapot.

Door overhitten van de honing vernietig je de enzymen en pollen welke in natuurlijke honing aanwezig zijn, in feite maak je dan een siroop van de honing. Maar honing mag het daarna niet meer genoemd worden. Overhitten kan de smaak ook veranderen.

Mocht je het nou lastig vinden om honing te verwarmen, dan kan je het gebruiken voor het bakken van een cake of het zoeten van je warme dranken zoals thee en koffie.