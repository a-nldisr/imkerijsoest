+++
date = 2021-02-12T23:00:00Z
title = "Voer ik mijn bijen suiker?"

+++
Het korte antwoord is: Ja, en door een trucje komt er geen suiker in de honing.

Het lange antwoord is:

Iedere imker in onze regio zal moeten bijvoeren als deze imker honing oogst, dat is de realiteit voor elke imker in de omgeving; Soest, Leusen of Amersfoort. De reden zijn onze de dracht periodes. Een dracht periode is een periode waarin uitzonderlijk veel nectar gehaald wordt door een bijenvolk, genoeg om als imker honing te kunnen oogsten.

**Dracht perioden**

Er zijn in onze regio 4 dracht periodes:

* Paardebloem en fruitbomen (voorjaar honing), April-Mei
* Acacia, eind Mei
* Lindenboom, eind Juni
* Heide, eind Augustus

In principe kan je op de honing van het acacia en lindehoning prima overwinteren. Dit zijn heldere honing varianten, meestal redelijk licht van kleur.

De honing van het voorjaar is honing welke ik meestal niet heel veel van oogst, dit is de periode waarin volken zwermen of ik nieuwe volken maak. Door deze handelingen gebruiken de volken de honing.

Acacia honing is iets wat niet gegarandeerd is, het moet daarvoor boven de 20 graden zijn in Mei en het mag niet regenen tijdens de bloei. Dit jaar had ik heel weinig Acacia honing.

De lindehoning is onze hoofddracht, sommige imkers winteren op deze honing in maar bij navraag blijkt vaak dat volken alsnog bijgevoerd moeten worden. Deze dracht duurt tot begin Juli, als er niets meer binnen komt aan honing dan is de periode tot Maart erg lang. Het houden van bijen om ze te laten verhongeren lijkt mij niet het doel van imker zijn.

Het process om bijenvolken klaar voor winter te maken noemen wij inwinteren, het is mogelijk om met heidehoning de winter in te gaan maar er zitten risico's aan. Heidehoning is thixotropisch, wat neerkomt op dat het vloeibaar is zodra het geroerd is, maar geleiachtig is zodra het een aantal dagen staat. Door deze eigenschap zitten er veel ballaststoffen in de honing, een honingbij gaat maanden de winter in en eet uitsluitend van de wintervoorraad. Als er veel ballaststoffen in de honing zitten kan er roer of nosema ontstaan, platgezegd een ziekte die schijterij veroorzaakt. Bijenvolken kunnen hier aan dood gaan.

**Suiker**

Suiker uit suikerbieten heeft geen ballaststoffen, door suikerwater te voeren geef je kwalitatief hoog voer aan de bijen voor de winter. Het mist een aantal bestanddelen zoals pollen, vitamines etc, maar als brandstof om je de winter door te rillen is het prima. Waar suiker slecht is voor mensen is dat niet het geval voor de honingbij, een honingbij is geen mens en reageert totaal anders dan dat wij dat doen. Honingbijen overwinteren hierdoor beter op suikerwater dan op eigen honing als deze honing veel ballaststoffen hebben. Soms is het dus beter om op suikerwater te overwinteren dan dat je dit zou doen met eigen honing.

**Trucje**

Gezien ik honing oogst van mijn bijenvolken, maar de gezondheid van mijn bijen wel voorop staat, voer ik suikerwater. De ramen honing welke bijenvolken overhouden na de winter geef ik mee aan jonge nieuwe bijenvolken. Op deze manier kan ik daardoor garanderen dat er geen suikerwater in de door mij verkochte honing zit. Jonge volken haal ik geen honing van af.