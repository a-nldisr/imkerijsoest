---
title: "Welkom"
date: 2020-09-11
menu:
  sidebar:
    name: Welkom!
    identifier: welkom
    weight: 1
---

Per vandaag is de website online, kom gerust af en toe langs om foto's, video's en informatie te lezen over het imkeren in Soest