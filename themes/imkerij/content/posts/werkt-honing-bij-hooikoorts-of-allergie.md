+++
date = 2021-02-02T23:00:00Z
title = "Werkt honing bij hooikoorts of allergie?"

+++
Het is een vraag welke ik geregeld krijg:

> Werkt honing om minder hooikoorts te krijgen of allergie?

Het is helaas een antwoord wat ik schuldig moet blijven. Van mensen hoor ik geregeld dat het helpt om honing uit de streek te nemen, ze ervaren minder hooikoorts klachten na geregeld consumeren van honing. Er zijn mensen die erbij zweren.

Zelf ben ik vorig jaar erachter gekomen dat ik hooikoorts heb en dat door graspollen veroorzaakt is. Elk jaar was ik altijd een beetje snotterig rond het voorjaar en had ik vrij milde klachten, maar vorig jaar heb ik voor de bijenstal op het Gagelgat voor het eerst gras hoeven maaien in mijn leven. Ik had ademtekort en jeukende ogen, dagenlang dacht ik dat ik een griep aan het krijgen was. De volgende keer na het maaien ervaarde ik dezelfde klachten en daarmee is het voor mij redelijk zeker dat ik hooikoorts heb door graspollen. Nu eet ik als imker geregeld honing en heb ik even veel last van hooikoorts.

De reden is dat gras geen bloemen heeft welke onze bijen bezoeken. Ik denk dat er heus wel iets aan pollen van gras verdwaald is in honing, maar genoeg om resistentie op te bouwen is het niet.

Ook heb ik wel eens vragen gekregen of suikerpatiënten honing mogen, ook daar moet ik het antwoord schuldig op blijven. 

Lang verhaal dus kort:

Ik zie honing niet als geneesmiddel. Het beste advies is nog altijd van de huisarts, niet die van een imker.