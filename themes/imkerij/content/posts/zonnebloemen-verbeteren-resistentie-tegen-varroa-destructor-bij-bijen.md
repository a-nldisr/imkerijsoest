+++
date = 2021-10-31T23:00:00Z
title = "Zonnebloemen verbeteren resistentie tegen Varroa Destructor bij bijen."

+++
Vandaag kwam ik een interessant paper tegen wat ik het vermelden waard vond:

[https://www.biorxiv.org/content/10.1101/2021.08.04.454914v1](https://www.biorxiv.org/content/10.1101/2021.08.04.454914v1 "https://www.biorxiv.org/content/10.1101/2021.08.04.454914v1")

In deze paper beschrijft men naar het effect wat zonnebloem pollen heeft op de hoeveelheid resistentie tegen Varroa Destructor in de bijenkolonies. Dit onderzoek hebben ze gedaan door het toedienen van pollen supplementen en het planten van zonnebloemen in de nabijheid.

## _Resultaten_

Resultaten lieten zien dat de mijten plaag met 28% verminderd werd. Gezien de zonnebloem bloeit tegen het einde van het bijenseizoen, op het moment waarop de aantallen varroa mijten het hoogst zijn, kan het planten van zonnebloemen in de omgeving voordelig zijn voor onze honingbijen. Daarnaast is het planten van de zonnebloem ook voor andere soorten bijen in de omgeving voordelig. Het was al bekend dat bij hommelsoort _Bombus impatiens_ zonnebloemen de pollen helpen bij de bestrijding van de parasiet _Crithidia bombi_ en dat zonnebloemen pollen kan helpen bij Nosema. 

## **_Er kunnen nadelen zijn_**

In het onderzoek vermelden ze ook dat zonnebloemen pollen een lage kwaliteit hebben, met als gevolg dat bijenbroed problemen kan ondervinden als te veel van dit soort pollen gevoerd wordt. Er moet dus nog meer onderzoek worden gedaan. Begin dus niet meteen met een hectares aan zonnebloem velden aan te leggen, maar het kan geen kwaad om een paar in de buurt van de bijenstal te zetten. Ook goed voor de hommels!