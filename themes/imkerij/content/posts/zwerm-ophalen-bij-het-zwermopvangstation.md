+++
date = 2021-06-16T22:00:00Z
title = "Zwerm ophalen bij het zwermopvangstation"

+++
**Wachtlijst**

Als je op de wachtlijst staat voor een zwerm kunnen we 2 dingen hebben afgesproken:

1. Je wilt een zwerm zonder ramen, ik bel je zodra deze binnen komt. Ik verwacht dat je binnen 24 uur deze zwerm kan komen ophalen. Anders gaat de zwerm in een kastje.
2. Je wilt een zwerm op spaar ramen, ik bel je zodra het volkje het eerste broed heeft. Indien je niets doorgeeft ga ik ervan uit dat je hiervoor kiest.

Zodra je aan de beurt bent zal ik je een bericht of e-mail sturen. Ik verwacht dat je binnen een paar dagen (6 max) het volk kan komen ophalen. Indien dat niet lukt zal je naar de volgende plek op de lijst gaan.

Als we contact hebben gehad zal ik je een volkje aanwijzen, dit volkje heeft 6 ramen en er moet minstens 2 ramen broed met wat gesloten broed in zitten wat er gezond uitziet. Daarnaast heeft het genoeg voer voor een paar dagen. Dit is de reden waarom ik je dit kastje aan wijs, andere kastjes zullen niet zo ver zijn of misschien een ongepaarde moer hebben. Om deze reden vraag ik je ook om niet zomaar in de kastjes te gaan zitten.

**Vergoeding**

Zoals je weet heeft onze vereniging geen winstoogmerk, maar moet het zwermopvangstation zich wel zelf kunnen draaiende houden. Om deze reden zijn er 2 opties voor een vergoeding van deze volken:

1\. Je neemt het volkje over en levert 6 nieuwe ingesmolten broedkamer ramen, daarnaast zijn de kosten 5 euro. (Geen gebruikte ramen, niets los, gewoon goed ingesmolten nieuwe broedkamer spaarkast ramen, zodat ik niet met de rommel zit opgescheept).

2\. Je neemt de 6 ramen over en betaald 25 euro zodat we daar weer nieuwe ramen van kunnen kopen.

Het geld gebruiken wij om kastjes te vervangen na een storm, ramen aan te schaffen en andere materialen zoals voer tijdens een drachtloze periode.

**Ophalen**

Over het algemeen ben ik niet aanwezig tijdens het ophalen van de volkjes en is het veelal zelf service, je moet even aangeven als je hulp nodig hebt.

Bij het ophalen zijn er twee mogelijkheden:

1. Je neemt het kastje mee, zet de ramen thuis over. Binnen 24 uur is het kastje weer terug zodat we niet achter mensen aan hoeven of een boekhouding moeten gaan bijhouden.
2. Je komt langs, zet je eigen 6 ramer neer op de plek waar het kastje stond en hangt de kastjes over. Rond schemer tijd kom je het kastje dicht doen en neem je het mee. Het kastje van de vereniging zet je daarna terug.

Vergeet niet een spanband te gebruiken en zorg dat de kast dicht zit!

Op alle kastjes liggen stenen tegen het af waaien van de deksels, zodra het volk meegenomen is leg je de steen weer terug.

**Waar kan ik het ophalen?**

Het zwermopvangstation staat op Birkstraat 107 bij het Gagelgat in Soest.

De volgende video legt uit hoe je bij het zwermopvangstation kan komen:

https://youtu.be/BT67r05kQK8

Naast het zwermopvangstation staan ook mijn en andere mensen hun kasten, ga hier aub niet in zitten rommelen (moet het kennelijk toch vermelden). Deze kasten zijn privé eigendom, daarnaast test ik elk jaar verschillende behandelingen en technieken en zodra je hier de boel in verstoord ben ik weken aan werk kwijt.

**Lidmaatschap**

Deze dienst is alleen beschikbaar voor leden van bijen vereniging Eemskwartier in Amersfoort, Leusden, Soest. Je kan mij benaderen via de Whatsapp groep van de vereniging.