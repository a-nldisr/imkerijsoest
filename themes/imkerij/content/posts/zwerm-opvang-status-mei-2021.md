+++
date = 2021-05-31T22:00:00Z
title = "Zwerm opvang status Mei 2021"

+++
Een korte update met betrekking tot onze zwerm opvangcentrum in Soest.

Dit jaar zijn er in de maand Mei meer zwermen dan vorig jaar opgevangen op het opvangcentrum. Uit eerste navraag blijkt dat dit vooral honger zwermen lijken te zijn. Bijen kunnen soms bij gebrek aan voer gaan zwermen. De zwermen zijn allemaal voorzien van wat suikerdeeg en gaan binnenkort naar imkers welke op de wachtlijst staan.

![](/uploads/zwermstation.jpg)